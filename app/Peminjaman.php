<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = "peminjaman";
    protected $fillable = [
        "buku_id",
        "anggota_id",
        "tgl_pinjam",
        "tgl_kembali",
        "status_pinjaman"
    ];

    public function buku()
    {
        return $this->belongsTo('App\Buku');
    }  
    public function anggota()
    {
        return $this->belongsTo('App\Anggota');
    }

}
