<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Anggota;
use App\Buku;
use App\Peminjaman;
use DB;
use PhpParser\Node\Expr\New_;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = Anggota::all();
        $buku = Buku::all();
        $peminjaman = Peminjaman::all();
       
        return view('page-admin.data-peminjaman', compact('anggota', 'buku', 'peminjaman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $anggota = Anggota::all();
        $buku = Buku::all();
       
        return view('page-admin.form-peminjaman', compact('anggota', 'buku'));
        
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'buku_id' => 'required',
            'anggota_id' => 'required',
            'tgl_pinjam' => 'required',
            'tgl_kembali' => 'required',
            'status_pinjaman' => 'required'
        ]);

        $peminjaman = new Peminjaman;
        
 
        $peminjaman->buku_id = $request->buku_id;
        $peminjaman->anggota_id = $request->anggota_id;
        $peminjaman->tgl_pinjam = $request->tgl_pinjam;
        $peminjaman->tgl_kembali = $request->tgl_kembali;
        $peminjaman->status_pinjaman = $request->status_pinjaman;

        $peminjaman->save();

        return redirect('/peminjaman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = DB::table('buku')->get();
        $anggota = DB::table('anggota')->get();
        $peminjaman = Peminjaman::find($id);

        return view('page-admin.edit-peminjaman', compact('buku', 'anggota', 'peminjaman'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'buku_id' => 'required',
            'anggota_id' => 'required',
            'tgl_pinjam' => 'required',
            'tgl_kembali' => 'required',
            'status_pinjaman' => 'required'
    	]
        
        );
            $peminjaman = Peminjaman::find($id);

            $peminjaman->buku_id = $request->buku_id;
            $peminjaman->anggota_id = $request->anggota_id;
            $peminjaman->tgl_pinjam = $request->tgl_pinjam;
            $peminjaman->tgl_kembali = $request->tgl_kembali;
            $peminjaman->status_pinjaman = $request->status_pinjaman;
        
    

        $peminjaman->update();
        return redirect('/peminjaman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
