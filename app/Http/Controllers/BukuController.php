<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jenis;

use App\Buku;

use DB;


class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        $jenis=Jenis::all();
        return view('page-admin.data-buku', compact('buku', 'jenis'));  
    }

    //view BUKU
    public function view()
    {
        $buku = Buku::all();
        $jenis = DB::table('jenis')->get();
        return view('page-admin.view-buku', compact('buku', 'jenis'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $jenis = Jenis::all();
       
        return view('page-admin.form-buku', compact('jenis')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required|max:255',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_perolehan' => 'required',
            'asal_perolehan' => 'required',
            'jumlah_buku' => 'required',
            'klasifikasi' => 'required',
            'kondisi_buku' => 'required',
            'jenis_id' => 'required',
            'thumbnail' =>  'image|mimes:jpeg,jpg,png|max:2048'
    	]
        );

        $ThumbnailName = time(). '.' .$request->thumbnail->extension();
        $request->thumbnail->move(public_path('gambar-buku'), $ThumbnailName);

        $buku = new Buku;
 
        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->penerbit = $request->penerbit;
        $buku->tahun_perolehan = $request->tahun_perolehan;
        $buku->asal_perolehan = $request->asal_perolehan;
        $buku->jumlah_buku = $request->jumlah_buku;
        $buku->klasifikasi = $request->klasifikasi;
        $buku->kondisi_buku = $request->kondisi_buku;
        $buku->jenis_id = $request->jenis_id;
        
        $buku->thumbnail = $ThumbnailName;
        
       
 
        $buku->save();

        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        
        $jenis = DB::table('jenis')->get();
        return view('page-admin.edit-buku', compact('buku', 'jenis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'judul' => 'required|max:255',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_perolehan' => 'required',
            'asal_perolehan' => 'required',
            'jumlah_buku' => 'required',
            'klasifikasi' => 'required',
            'kondisi_buku' => 'required',   
            'jenis_id' => 'required',
            'thumbnail' =>  'image|mimes:jpeg,jpg,png|max:2048'
    	]
        
        );
        $buku = Buku::find($id);

        if  ($request->has('thumbnail')){

            $ThumbnailName = time(). '.' .$request->thumbnail->extension();
            $request->thumbnail->move(public_path('gambar-buku'), $ThumbnailName);

            $buku->judul = $request->judul;
            $buku->pengarang = $request->pengarang;
            $buku->penerbit = $request->penerbit;
            $buku->tahun_perolehan = $request->tahun_perolehan;
            $buku->asal_perolehan = $request->asal_perolehan;
            $buku->jumlah_buku = $request->jumlah_buku;
            $buku->klasifikasi = $request->klasifikasi;
            $buku->kondisi_buku = $request->kondisi_buku;
            $buku->jenis_id = $request->jenis_id;
            
            $buku->thumbnail = $ThumbnailName;

        } else{
            $buku->judul = $request->judul;
            $buku->pengarang = $request->pengarang;
            $buku->penerbit = $request->penerbit;
            $buku->tahun_perolehan = $request->tahun_perolehan;
            $buku->asal_perolehan = $request->asal_perolehan;
            $buku->jumlah_buku = $request->jumlah_buku;
            $buku->klasifikasi = $request->klasifikasi;
            $buku->kondisi_buku = $request->kondisi_buku;
            $buku->jenis_id = $request->jenis_id;

        }

        $buku ->update();
        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
