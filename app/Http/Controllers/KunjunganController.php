<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Anggota;
use App\Kunjungan;
use DB;

class KunjunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kunjungan = Kunjungan::all();
        return view('page-admin.data-kunjungan', compact('kunjungan')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $anggota = Anggota::all();
       
        return view('page-admin.form-kunjungan', compact('anggota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'tgl_kunjungan' => 'required',
            'ket' => 'required',
            'anggota_id' => 'required'
            
    	]
        );

        
        $kunjungan = new Kunjungan;
 
        $kunjungan->tgl_kunjungan = $request->tgl_kunjungan;
        $kunjungan->ket = $request->ket;
        $kunjungan->anggota_id = $request->anggota_id;
        
       
 
        $kunjungan->save();

        return redirect('/kunjungan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kunjungan = Kunjungan::find($id);
        $anggota = DB::table('anggota')->get();
        return view('page-admin.edit-kunjungan', compact('kunjungan', 'anggota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'tgl_kunjungan' => 'required',
            'ket' => 'required',
            'anggota_id' => 'required'
    	]
        
        );
            $kunjungan = Kunjungan::find($id);

            $kunjungan->tgl_kunjungan = $request->tgl_kunjungan;
            $kunjungan->ket = $request->ket;
            $kunjungan->anggota_id = $request->anggota_id;
        
    

        $kunjungan->update();
        return redirect('/kunjungan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kunjungan = Kunjungan::find($id);
        $kunjungan -> delete();
        return redirect('/kunjungan');
    }
}
