<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anggota;

use App\Kunjungan;


class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = Anggota::all();
        return view('page-admin.data-anggota', compact('anggota'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page-admin.form-anggota');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama_anggota' => 'required|max:255',
            'tgl_daftar' => 'required',
            'no_anggota' => 'required',
            'status_anggota' => 'required',
            
            'thumbnail' =>  'image|mimes:jpeg,jpg,png|max:2048'
    	]
        );

        $ThumbnailName = time(). '.' .$request->thumbnail->extension();
        $request->thumbnail->move(public_path('foto-anggota'), $ThumbnailName);

        $anggota = new Anggota;
 
        $anggota->nama_anggota = $request->nama_anggota;
        $anggota->tgl_daftar = $request->tgl_daftar;
        $anggota->no_anggota = $request->no_anggota;
        $anggota->status_anggota = $request->status_anggota;
        
        $anggota->foto = $ThumbnailName;
        
       
 
        $anggota->save();

        return redirect('/anggota');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anggota = Anggota::find($id);
        $kunjungan = Kunjungan::find($id);
        return view('page-admin.show-anggota', compact('anggota', 'kunjungan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggota = Anggota::find($id);
        return view('page-admin.edit-anggota', compact('anggota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'nama_anggota' => 'required|max:255',
            'tgl_daftar' => 'required',
            'no_anggota' => 'required',
            'status_anggota' => 'required',
            'foto' =>  'image|mimes:jpeg,jpg,png|max:2048'
    	]
        
        );
        $anggota = Anggota::find($id);

        if  ($request->has('foto')){

            $ThumbnailName = time(). '.' .$request->foto->extension();
            $request->foto->move(public_path('foto-anggota'), $ThumbnailName);

            $anggota->nama_anggota = $request->nama_anggota;
            $anggota->tgl_daftar = $request->tgl_daftar;
            $anggota->no_anggota = $request->no_anggota;
            $anggota->status_anggota = $request->status_anggota;
            $anggota->foto = $ThumbnailName;

        } else{
            $anggota->nama_anggota = $request->nama_anggota;
            $anggota->tgl_daftar = $request->tgl_daftar;
            $anggota->no_anggota = $request->no_anggota;
            $anggota->status_anggota = $request->status_anggota;

        }

        $anggota ->update();
        return redirect('/anggota');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
        
        // $anggota = Anggota::find($id);
        // $anggota -> delete();
        // return redirect('/anggota');
    }
}
