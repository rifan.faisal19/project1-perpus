<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kunjungan extends Model
{
    protected $table = "kunjungan";
    protected $fillable = [
        "tgl_kunjungan",
        "ket",
        "anggota_id"
    ];


    public function anggota(){
        return $this->belongsTo('App\Anggota');
    }
}
