<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $table = "anggota";
    protected $fillable = [
        "nama_anggota",
        "tgl_daftar",
        "no_anggota", 
        "status_anggota", 
        "foto"

    ];

    public function kunjungan(){
        return $this->hasMany('App\Kunjungan');
    }

    public function peminjaman(){
        return $this->hasMany('App\Peminjaman');
    }
}
