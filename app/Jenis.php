<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    protected $table = "jenis";
    protected $fillable = ["nama_jenis"];


    public function buku(){
        return $this->belongsTo('App\Buku');
    }

}


