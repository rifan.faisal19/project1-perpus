<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";
    protected $fillable = [
        "judul",
        "pengarang",
        "penerbit", 
        "tahun_perolehan",
        "asal_perolehan",
        "jumlah_buku",
        "klasifikasi",
        "kondisi_buku", 
        "thumbnail", 
        "jenis_id"
    ];


    public function jenis(){
        return $this->hasOne('App\Jenis');
    }

    public function jenis1()
    {
    return $this->belongsTo('App\Jenis', 'jenis_id');
    }

    public function peminjaman(){
        return $this->hasMany('App\Peminjaman');
    }
}
