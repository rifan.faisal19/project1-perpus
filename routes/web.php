<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Route::group(['middleware' => ['auth']], function () {
    
        //DASHBOARD
        

        Route::get('/index', 'IndexController@index'); //tampilan awal

        //ANGGOTA
        Route::get('/anggota', 'AnggotaController@index'); //tampilan awal

        Route::get('/anggota/create', 'AnggotaController@create'); //form tambah 

        Route::post('/anggota', 'AnggotaController@store' ); //menyimpan 

        Route::get('/anggota/{anggota_id}', 'AnggotaController@show');

        Route::get('/anggota/{anggota_id}/edit', 'AnggotaController@edit');

        Route::put('/anggota/{anggota_id}', 'AnggotaController@update');

        Route::delete('/anggota/{anggota_id}', 'AnggotaController@destroy');


        //KUNJUNGAN
        Route::get('/kunjungan', 'KunjunganController@index'); //tampilan awal

        Route::get('/kunjungan/create', 'KunjunganController@create'); //form tambah 

        Route::post('/kunjungan', 'KunjunganController@store' ); //menyimpan 

        Route::get('/kunjungan/{kunjungan_id}', 'KunjunganController@show');

        Route::get('/kunjungan/{kunjungan_id}/edit', 'KunjunganController@edit');

        Route::put('/kunjungan/{kunjungan_id}', 'KunjunganController@update');

        Route::delete('/kunjungan/{kunjungan_id}', 'KunjunganController@destroy');

        //JENIS
        Route::get('/jenis', 'JenisController@index'); //tampilan awal

        Route::get('/jenis/create', 'JenisController@create'); //form tambah 

        Route::post('/jenis', 'JenisController@store' ); //menyimpan 

        Route::get('/jenis/{jenis_id}', 'JenisController@show');

        Route::get('/jenis/{jenis_id}/edit', 'JenisController@edit');

        Route::put('/jenis/{jenis_id}', 'JenisController@update');

        Route::delete('/jenis/{jenis_id}', 'JenisController@destroy');


        //BUKU
        Route::get('/buku', 'BukuController@index'); //tampilan awal

        Route::get('/buku/view', 'BukuController@view'); //tampilan awal

        Route::get('/buku/create', 'BukuController@create'); //form tambah 

        Route::post('/buku', 'BukuController@store' ); //menyimpan 

        Route::get('/buku/{buku_id}', 'BukuController@show');

        Route::get('/buku/{buku_id}/edit', 'BukuController@edit');

        Route::put('/buku/{buku_id}', 'BukuController@update');

        Route::delete('/buku/{buku_id}', 'BukuController@destroy');



        //PEMINJAMAN
        Route::get('/peminjaman', 'PeminjamanController@index'); //tampilan awal

        Route::get('/peminjaman/view', 'PeminjamanController@view'); //tampilan awal

        Route::get('/peminjaman/create', 'PeminjamanController@create'); //form tambah 

        Route::post('/peminjaman', 'PeminjamanController@store' ); //menyimpan 

        Route::get('/peminjaman/{peminjaman_id}', 'PeminjamanController@show');

        Route::get('/peminjaman/{peminjaman_id}/edit', 'PeminjamanController@edit');

        Route::put('/peminjaman/{peminjaman_id}', 'PeminjamanController@update');

        Route::delete('/peminjaman/{peminjaman_id}', 'PeminjamanController@destroy');


        

});

Auth::routes();