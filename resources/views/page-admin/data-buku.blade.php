@extends('page-admin.master')

@section('title')

Daftar Buku
    
@endsection

@section('content')



<nav class="navbar navbar-light ">
  <a href="/buku/create" class="btn btn-primary " > Tambah Buku Baru</a>
  <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-light my-2 my-sm-0" type="submit">
      <i class="fa fa-search" aria-hidden="true"></i>
    </button>
  </form>
</nav>



<div class="card-body">
<table class="table table-hover">
    <thead class="table-primary">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Judul</th>
        <th scope="col">Klasifikasi</th>
        <th scope="col">Jumlah Buku</th>
        <th scope="col">Jenis Buku</th>
        <th scope="col" style="width: 5%">Kondisi</th>
        <th scope="col" style="width: 16%">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($buku as $key=>$value)
        <tr>
            <td> {{$key + 1}} </td>
            <td> {{$value->judul}} </td>
            <td> {{$value->klasifikasi}} </td>
            <td> {{$value->jumlah_buku}} </td>
            <td> {{$value->jenis1->nama_jenis}} </td>
            <td> 
                  @if ($value->kondisi_buku === "Baik")
                  <h5><span class="badge badge-success"> {{$value->kondisi_buku}} </span></h5>
                  @elseif ($value->kondisi_buku === "Cukup Baik")
                  <h5><span class="badge badge-warning"> {{$value->kondisi_buku}} </span></h5>
                  @elseif ($value->kondisi_buku === "Rusak")
                  <h5><span class="badge badge-danger"> {{$value->kondisi_buku}} </span></h5>
                  @elseif ($value->kondisi_buku === "Hilang")
                  <h5><span class="badge badge-info"> {{$value->kondisi_buku}} </span></h5>
                  @else
                    Tidak ada status
                  @endif
             </td>
            <td> 

              {{-- <form action="/anggota/{{$value->id}}" method="POST">
                @csrf --}}


                  <a href="/buku/{{$value->id}}" class="btn btn-sm bg-info">
                    <i class="fas fa-info"></i> info
                  <a></a>

                  <a href="/buku/{{$value->id}}/edit" class="btn btn-sm bg-primary">
                    <i class="fa fa-cog fa-spin fa-1x"></i> Ubah
                  
                  {{-- @method('DELETE')
                  <input type="submit" class="btn btn-danger" value="Hapus">
              </form> --}}
                
                
            </td> 
            
        </tr>
            
            
        @empty
            
        @endforelse
    </tbody>
  </table>
</div>
@endsection