@extends('page-admin.master')

@section('title')

Form Edit Anggota
    
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

<form action="/anggota/{{$anggota->id}}" method="POST" enctype="multipart/form-data">
    @csrf   
    @method('PUT')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Biodata</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="form-group">
                    <label>Nomor Anggota</label>
                        <input type="number" class="form-control" name="no_anggota" placeholder="Masukkan NISN Siswa" value="{{$anggota->no_anggota}}">
                        @error('no_anggota')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Nama Anggota</label>
                        <input type="text" class="form-control form-control-lg" name="nama_anggota" placeholder="Nama Anggota" value="{{$anggota->nama_anggota}}">
                        @error('nama_anggota')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Tanggal Daftar</label>
                        <input type="date" class="form-control" name="tgl_daftar" placeholder="Tanggal Kelahiran" value="{{$anggota->tgl_daftar}}">
                        @error('tgl_daftar')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>
                 
                    <div class="form-group">
                        <label>Status Keanggotaan</label>
                        <select class="form-control" name="status_anggota">
                          <option value="">--Pilih Status--</option>
                          @if ($anggota->status_anggota === "Active")
                            <option value="Active" selected>Active</option>
                            <option value="Not Active">Not Active</option>
                          @elseif ($anggota->status_anggota === "Not Active")
                            <option value="Active">Active</option>
                            <option value="Not Active" selected>Not Active</option>
                        
                          @else
                                  Tidak ada status
                          @endif
                        </select>
                        @error('status_anggota')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                   
            <!-- general form elements disabled -->
            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Upload Photo</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                    <label>File input Photo</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="foto">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                    
                </div>
                  <div>
                
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>  
              </div>
              <!-- /.card-body -->  
    </section>
    <!-- /.content -->
</form>

@endsection