@extends('page-admin.master')

@section('title')

Form Edit Buku
    
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

<form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
    @csrf   
    @method('PUT')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit Buku </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="form-group">
                    <label>Judul Buku</label>
                        <input type="text" class="form-control form-control-lg" name="judul" value="{{$buku->judul}}">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Pengarang</label>
                        <input type="text" class="form-control" name="pengarang"  value="{{$buku->pengarang}}">
                        @error('pengarang')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Penerbit</label>
                        <input type="text" class="form-control" name="penerbit"  value="{{$buku->penerbit}}">
                        @error('penerbit')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Tahun Perolehan</label>
                        <input type="number" min="0" maxlength="4" class="form-control" name="tahun_perolehan" value="{{$buku->tahun_perolehan}}">
                        @error('penerbit')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>
                 
                    <div class="form-group">
                        <label>Asal Perolehan </label>
                        <select class="form-control" name="asal_perolehan">
                          <option value="">--Pilih Asal Perolehan--</option>
                                @if ($buku->asal_perolehan === "Dana BOS Regular")
                                <option value="Dana BOS Regular" selected>Dana BOS Regular</option>
                                <option value="Dana BOS Afirmasi">Dana BOS Afirmasi</option>
                                <option value="Dana BOS Kinerja">Dana BOS Kinerja</option>
                                <option value="Dana Alokasi Khusus (DAK)">Dana Alokasi Khusus (DAK)</option>
                                <option value="Dana Komite">Dana Komite</option>
                                <option value="Dana Lainnya">Dana Lainnya</option>

                                @elseif ($buku->asal_perolehan === "Dana BOS Afirmasi")
                                <option value="Dana BOS Regular">Dana BOS Regular</option>
                                <option value="Dana BOS Afirmasi" selected>Dana BOS Afirmasi</option>
                                <option value="Dana BOS Kinerja">Dana BOS Kinerja</option>
                                <option value="Dana Alokasi Khusus (DAK)">Dana Alokasi Khusus (DAK)</option>
                                <option value="Dana Komite">Dana Komite</option>
                                <option value="Dana Lainnya">Dana Lainnya</option>

                                @elseif ($buku->asal_perolehan === "Dana BOS Kinerja")
                                <option value="Dana BOS Regular">Dana BOS Regular</option>
                                <option value="Dana BOS Afirmasi">Dana BOS Afirmasi</option>
                                <option value="Dana BOS Kinerja" selected>Dana BOS Kinerja</option>
                                <option value="Dana Alokasi Khusus (DAK)">Dana Alokasi Khusus (DAK)</option>
                                <option value="Dana Komite">Dana Komite</option>
                                <option value="Dana Lainnya">Dana Lainnya</option>

                                @elseif ($buku->asal_perolehan === "Dana Alokasi Khusus (DAK)")
                                <option value="Dana BOS Regular">Dana BOS Regular</option>
                                <option value="Dana BOS Afirmasi">Dana BOS Afirmasi</option>
                                <option value="Dana BOS Kinerja">Dana BOS Kinerja</option>
                                <option value="Dana Alokasi Khusus (DAK)" selected>Dana Alokasi Khusus (DAK)</option>
                                <option value="Dana Komite">Dana Komite</option>
                                <option value="Dana Lainnya">Dana Lainnya</option>

                                @elseif ($buku->asal_perolehan === "Dana Komite")
                                <option value="Dana BOS Regular">Dana BOS Regular</option>
                                <option value="Dana BOS Afirmasi">Dana BOS Afirmasi</option>
                                <option value="Dana BOS Kinerja">Dana BOS Kinerja</option>
                                <option value="Dana Alokasi Khusus (DAK)">Dana Alokasi Khusus (DAK)</option>
                                <option value="Dana Komite" selected>Dana Komite</option>
                                <option value="Dana Lainnya">Dana Lainnya</option>

                                @elseif ($buku->asal_perolehan === "Dana Lainnya")
                                <option value="Dana BOS Regular">Dana BOS Regular</option>
                                <option value="Dana BOS Afirmasi">Dana BOS Afirmasi</option>
                                <option value="Dana BOS Kinerja">Dana BOS Kinerja</option>
                                <option value="Dana Alokasi Khusus (DAK)">Dana Alokasi Khusus (DAK)</option>
                                <option value="Dana Komite">Dana Komite</option>
                                <option value="Dana Lainnya" selected>Dana Lainnya</option>
                            
                                @else
                                        Tidak ada status
                                @endif
                        </select>
                        @error('asal_perolehan')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                      <label>Jumlah Buku</label>
                          <input type="number" class="form-control" name="jumlah_buku" value="{{$buku->jumlah_buku}}">
                          @error('jumlah_buku')
                              <div class="alert alert-danger">
                                  {{ $message }}
                              </div>
                          @enderror
                    </div>

                    <div class="form-group">
                      <label>Klasifikasi</label>
                      <select class="form-control" name="klasifikasi">
                        <option value="">--Pilih Klasifikasi--</option>
                        @if ($buku->klasifikasi === "Umum")
                            <option value="Umum" selected>Umum</option>
                            <option value="Kelas 1">Kelas 1</option>
                            <option value="Kelas 2">Kelas 2</option>
                            <option value="Kelas 3">Kelas 3</option>
                            <option value="Kelas 4">Kelas 4</option>
                            <option value="Kelas 5">Kelas 5</option>
                            <option value="Kelas 6">Kelas 6</option>


                        @elseif ($buku->klasifikasi === "Kelas 1")
                            <option value="Umum">Umum</option>
                            <option value="Kelas 1" selected>Kelas 1</option>
                            <option value="Kelas 2">Kelas 2</option>
                            <option value="Kelas 3">Kelas 3</option>
                            <option value="Kelas 4">Kelas 4</option>
                            <option value="Kelas 5">Kelas 5</option>
                            <option value="Kelas 6">Kelas 6</option>

                        @elseif ($buku->klasifikasi === "Kelas 2")
                            <option value="Umum">Umum</option>
                            <option value="Kelas 1">Kelas 1</option>
                            <option value="Kelas 2" selected>Kelas 2</option>
                            <option value="Kelas 3">Kelas 3</option>
                            <option value="Kelas 4">Kelas 4</option>
                            <option value="Kelas 5">Kelas 5</option>
                            <option value="Kelas 6">Kelas 6</option>
                        
                        @elseif ($buku->klasifikasi === "Kelas 3")
                            <option value="Umum">Umum</option>
                            <option value="Kelas 1">Kelas 1</option>
                            <option value="Kelas 2">Kelas 2</option>
                            <option value="Kelas 3" selected>Kelas 3</option>
                            <option value="Kelas 4">Kelas 4</option>
                            <option value="Kelas 5">Kelas 5</option>
                            <option value="Kelas 6">Kelas 6</option>

                        @elseif ($buku->klasifikasi === "Kelas 4")
                            <option value="Umum">Umum</option>
                            <option value="Kelas 1">Kelas 1</option>
                            <option value="Kelas 2">Kelas 2</option>
                            <option value="Kelas 3">Kelas 3</option>
                            <option value="Kelas 4" selected>Kelas 4</option>
                            <option value="Kelas 5">Kelas 5</option>
                            <option value="Kelas 6">Kelas 6</option>
                        
                        @elseif ($buku->klasifikasi === "Kelas 5")
                            <option value="Umum">Umum</option>
                            <option value="Kelas 1">Kelas 1</option>
                            <option value="Kelas 2">Kelas 2</option>
                            <option value="Kelas 3">Kelas 3</option>
                            <option value="Kelas 4">Kelas 4</option>
                            <option value="Kelas 5" selected>Kelas 5</option>
                            <option value="Kelas 6">Kelas 6</option>
                        
                        @elseif ($buku->klasifikasi === "Kelas 6")
                            <option value="Umum">Umum</option>
                            <option value="Kelas 1">Kelas 1</option>
                            <option value="Kelas 2">Kelas 2</option>
                            <option value="Kelas 3">Kelas 3</option>
                            <option value="Kelas 4">Kelas 4</option>
                            <option value="Kelas 5">Kelas 5</option>
                            <option value="Kelas 6" selected>Kelas 6</option>

                        @else
                            Tidak ada status
                        @endif
                        
                      </select>
                          @error('klasifikasi') 
                              <div class="alert alert-danger">
                                  {{ $message }}
                              </div>
                          @enderror
                    </div>

                    <div class="form-group">
                      <label>Kondisi Buku </label>
                      <select class="form-control" name="kondisi_buku">
                        <option value="">--Pilih Kondisi Buku--</option>
                        @if ($buku->kondisi_buku === "Baik")
                            <option value="Baik" selected>Baik</option>
                            <option value="Cukup Baik">Cukup Baik</option>
                            <option value="Rusak">Rusak</option>
                            <option value="Hilang">Hilang</option>

                        @elseif($buku->kondisi_buku === "Cukup Baik")
                            <option value="Baik">Baik</option>
                            <option value="Cukup Baik" selected>Cukup Baik</option>
                            <option value="Rusak">Rusak</option>
                            <option value="Hilang">Hilang</option>

                        @elseif($buku->kondisi_buku === "Rusak")
                            <option value="Baik">Baik</option>
                            <option value="Cukup Baik">Cukup Baik</option>
                            <option value="Rusak" selected>Rusak</option>
                            <option value="Hilang">Hilang</option>

                        @elseif($buku->kondisi_buku === "Hilang")
                            <option value="Baik">Baik</option>
                            <option value="Cukup Baik">Cukup Baik</option>
                            <option value="Rusak">Rusak</option>
                            <option value="Hilang" selected>Hilang</option>
                        @else
                            Tidak ada status
                        @endif  

                      </select>
                      @error('kondisi_buku')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                      @enderror
                  </div>

                  <div class="form-group">
                    <label>Jenis Buku </label>
                    <select class="form-control" name="jenis_id">
                      <option value="">--Pilih Jenis Buku--</option>
                        @foreach ($jenis as $item)
                            @if ($item->id === $buku->jenis_id)
                                <option value="{{$item->id}}" selected>{{$item->nama_jenis}}</option>
                            @else
                                <option value="{{$item->id}}">{{$item->nama_jenis}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('jenis_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                   
            <!-- general form elements disabled -->
            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Upload Photo</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                    <label>File input Cover Buku</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="thumbnail">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                </div>
                  <div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>  
              </div>
              <!-- /.card-body -->  
    </section>
    <!-- /.content -->
</form>

@endsection