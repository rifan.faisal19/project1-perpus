@extends('page-admin.master')

@section('title')

Halaman Edit Kunjungan 
    
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

<form action="/kunjungan/{{$kunjungan->id}}" method="POST" enctype="multipart/form-data">
    @csrf   
    @method('PUT')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit Kunjungan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">

                    <div class="form-group">
                        <label>Tanggal Kunjungan</label>
                            <input type="date" class="form-control" name="tgl_kunjungan" value="{{$kunjungan->tgl_kunjungan}}">
                            @error('tgl_kunjungan')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                    </div>
                    
                  <div class="form-group">
                    <label>Pilih Anggota Yang Berkunjung </label>
                        <select class="form-control" name="anggota_id">
                            @foreach ($anggota as $item)
                                @if ($item->id === $kunjungan->anggota_id)
                                    <option value="{{$item->id}}" selected>{{$item->nama_anggota}}</option>
                                @else
                                    <option value="{{$item->id}}">{{$item->nama_anggota}}</option>
                                @endif
                            @endforeach
                                
                            
                        </select>
                        @error('anggota_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Keterangan</label>
                        <textarea class="form-control form-control-lg" name="ket">{{$kunjungan->ket}}</textarea>
                        @error('nama_anggota')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                 
                    </div>
                    
                </div>
                  <div>
                
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>  
              </div>
              <!-- /.card-body -->  
    </section>
    <!-- /.content -->
</form>

@endsection