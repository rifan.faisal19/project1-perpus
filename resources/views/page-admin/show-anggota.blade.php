@extends('page-admin.master')

@section('title')

Profile Anggota
    
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
 
    
    <!-- Main content -->
   
     
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                      src="{{asset('foto-anggota/'. $anggota->foto)}}"
                       alt="User profile picture">
                </div>

                <b><h3 class="profile-username text-center">{{$anggota->nama_anggota}}</h3></b>

                <p class="text-muted text-center">Anggota Perpustakaan</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>No. Anggota <a class="float-right">{{$anggota->no_anggota}}</b></a>
                  </li>
                  <li class="list-group-item">
                    <b>Tgl Daftar <a class="float-right">{{$anggota->tgl_daftar}}</b></a>
                  </li>
                  <li class="list-group-item">
                    <b>Status <a class="float-right">
                      @if ($anggota->status_anggota === "Active")
                      <h5><span class="badge badge-success"> {{$anggota->status_anggota}} </span></h5>
                      @elseif ($anggota->status_anggota === "Not Active")
                      <h5><span class="badge badge-danger"> {{$anggota->status_anggota}} </span></h5>
                      @else
                        Tidak ada status
                      @endif
                    
                    </b>
                    </a>
                  </li>
                </ul>

                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  {{-- <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li> --}}
                  <li class="nav-item"><a class="nav-link bg-primary" href="#timeline" data-toggle="tab">Timeline</a></li>
                  {{-- <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li> --}}
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  
                  <!-- /.tab-pane -->
                  <div class="active tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                      <!-- timeline time label -->
                      <div class="time-label">
                        <span class="bg-danger">
                          {{$anggota->tgl_daftar}}
                        </span>
                      </div>
                      <!-- /.timeline-label -->
                      <!-- timeline item -->
                      
                      <div>
                        <i class="fas fa-user bg-danger"></i>

                        <div class="timeline-item">
                          
                          <h3 class="timeline-header border-0"><a href="#">{{$anggota->nama_anggota}}</a> diterima menjadi anggota
                          </h3>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      
                     
                    </div>


                    @foreach ($anggota->kunjungan as $item)
                        <div class="timeline timeline-inverse">
                     
                        <div class="time-label">
                          <span class="bg-info">
                            {{$item->tgl_kunjungan}}
                          </span>
                        </div>
                        <div>
                          <i class="fas fa-book-reader bg-info"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header border-0"><a href="#">{{$item->Anggota->nama_anggota}}</a> Melakukan kunjunan ke Perpustakaan 
                            </h3>
                          </div>
                        </div>
                    </div>  
                    @endforeach

                    @foreach ($anggota->peminjaman as $item)
                        @if ($item->status_pinjaman === "Belum Kembali")

                            <div class="timeline timeline-inverse">
                              <div class="time-label">
                                <span class="bg-warning">
                                  {{$item->tgl_pinjam}}
                                </span>
                              </div>  
                              <div>
                                <i class="fas fa-book bg-warning"></i>
                                <div class="timeline-item">
                                  <h3 class="timeline-header border-0"><a href="#">{{$item->Anggota->nama_anggota}}</a> Melakukan Peminjaman buku {{$item->Buku->judul}}
                                  </h3>
                                </div>
                              </div>
                            </div>

                        @elseif ($item->status_pinjaman === "Sudah Kembali")

                            <div class="timeline timeline-inverse">
                              <div class="time-label">
                                <span class="bg-success">
                                  {{$item->tgl_kembali}}
                                </span>
                              </div>  
                              <div>
                                <i class="fas fa-book bg-success"></i>
                                <div class="timeline-item">
                                  <h3 class="timeline-header border-0"><a href="#">{{$item->Anggota->nama_anggota}}</a> telah melakukan Pengembalian buku {{$item->Buku->judul}}
                                  </h3>
                                </div>
                              </div>
                            </div>
                        
                        @else
                              Tidak ada status
                        @endif

                          
                    @endforeach
                  </div>
                
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
      
    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->


@endsection



