@extends('page-admin.master')

@section('title')

Daftar Peminjam Buku
    
@endsection

@section('content')



<nav class="navbar navbar-light ">
  <a href="/peminjaman/create" class="btn btn-primary " > Input Transaksi Baru</a>
  <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-light my-2 my-sm-0" type="submit">
      <i class="fa fa-search" aria-hidden="true"></i>
    </button>
  </form>
</nav>



<div class="card-body">
<table class="table table-hover">
    <thead class="table-primary">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Tgl Pinjam</th>
        <th scope="col">Nama Peminjam</th>
        <th scope="col">Buku Yang diPinjam</th>
        <th scope="col">Tgl Kembali</th>
        <th scope="col">Status Pinjaman</th>
        <th scope="col" style="width: 16%">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($peminjaman as $key=>$value)
        <tr>
            <td> {{$key + 1}} </td>
            <td> {{$value->tgl_pinjam}}</td>  
            <td> {{$value->anggota->nama_anggota}} </td>
            <td> {{$value->buku->judul}} </td>  
            <td> {{$value->tgl_kembali}} </td>
            <td> 
                @if ($value->status_pinjaman === "Belum Kembali")
                    <h5><span class="badge badge-warning"> {{$value->status_pinjaman}} </span></h5>
                @elseif ($value->status_pinjaman === "Sudah Kembali")
                    <h5><span class="badge badge-success"> {{$value->status_pinjaman}} </span></h5>
                @else
                  Tidak ada status
                @endif
            </td>  
            <td> 

              <form action="/peminjaman/{{$value->id}}" method="POST">
                @csrf
                  
                  <a href="/peminjaman/{{$value->id}}/edit" class="btn btn-sm bg-primary" >
                    <i class="fa fa-cog fa-spin fa-1x "></i> Ubah

                    <a></a>
                  
                  @method('DELETE')
                  <input type="submit" class="btn btn-sm bg-danger" value="Hapus">
              </form>
                
                
            </td> 
            
        </tr>
            
            
        @empty
            
        @endforelse
    </tbody>
  </table>
</div>
@endsection