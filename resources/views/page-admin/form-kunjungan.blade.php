@extends('page-admin.master')

@section('title')

Form Kunjungan
    
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

<form action="/kunjungan" method="POST" enctype="multipart/form-data">
    @csrf   

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Input Kunjungan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">

                    <div class="form-group">
                        <label>Tanggal Kunjungan</label>
                            <input type="date" class="form-control" name="tgl_kunjungan" >
                            @error('tgl_kunjungan')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                    </div>
                    
                  <div class="form-group">
                    <label>Pilih Anggota Yang Berkunjung </label>
                        <select class="form-control" name="anggota_id">
                            <option value="">--Pilih Siswa--</option>
                            @foreach ($anggota as $item)
                            <option value="{{$item->id}}">{{$item->nama_anggota}}</option>
                            @endforeach
                        </select>
                        @error('anggota_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Keterangan</label>
                        <textarea class="form-control form-control-lg" name="ket"></textarea>
                        @error('ket')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                 
                    </div>
                    
                </div>
                  <div>
                
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>  
              </div>
              <!-- /.card-body -->  
    </section>
    <!-- /.content -->
</form>

@endsection