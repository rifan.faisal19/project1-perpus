@extends('page-admin.master')

@section('title')

Daftar Kunjungan
    
@endsection

@section('content')



<nav class="navbar navbar-light ">
  <a href="/kunjungan/create" class="btn btn-primary " > Input Kunjungan Baru</a>
  <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-light my-2 my-sm-0" type="submit">
      <i class="fa fa-search" aria-hidden="true"></i>
    </button>
  </form>
</nav>



<div class="card-body">
<table class="table table-hover">
    <thead class="table-primary">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Pengunjung</th>
        <th scope="col">Tanggal Kunjungan</th>
        <th scope="col">Keterangan</th>
        <th scope="col" style="width: 16%">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kunjungan as $key=>$value)
        <tr>
            <td> {{$key + 1}} </td>
            <td> {{$value->Anggota->nama_anggota}} </td>
            <td> {{$value->tgl_kunjungan}} </td>
            <td> {{$value->ket}} </td>  
            <td> 

              <form action="/kunjungan/{{$value->id}}" method="POST">
                @csrf
                  
                  <a href="/kunjungan/{{$value->id}}/edit" class="btn btn-sm bg-primary">
                    <i class="fa fa-cog fa-spin fa-1x "></i> Ubah

                    <a></a>
                  
                  @method('DELETE')
                  <input type="submit" class="btn btn-sm bg-danger" value="Hapus">
              </form>
                
                
            </td> 
            
        </tr>
            
            
        @empty
            
        @endforelse
    </tbody>
  </table>
</div>
@endsection