@extends('page-admin.master')

@section('title')

Daftar Koleksi Buku 
    
@endsection

@section('content')

<section class="content">

    <!-- Default box -->
    <div class="card card-solid">
      <div class="card-body pb-0">
        <div class="row">
            @foreach($buku as $key=>$item)
                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column ">
                    <div class="card bg-light d-flex flex-fill">
                    <div class="card-header text-muted border-bottom-0">
                        <label class="text-info">{{$item->klasifikasi}}</label>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-7">
                                <label class="text-primary"><h2 class="lead"><b>{{$item->judul}}</b></h2></label>
                                <p class="text-muted text-sm"><b>Pengarang: </b>{{$item->pengarang}} </p>
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Penerbit: {{$item->penerbit}}</li>
                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-bookmark"></i></span> Asal : {{$item->asal_perolehan}}</li>
                                <li class="small"><span class="fa-li"><i class="fas fa-calendar-alt"></i></span> Tahun :  {{$item->tahun_perolehan}}</li>
                                <li class="small"><span class="fa-li"><i class="fas fa-plus"></i></span> Jumlah Buku: <b> {{$item->jumlah_buku}} </b></li>
                                <li class="small"><span class="fa-li"><i class="fas fa-book"></i></span> {{$item->jenis1->nama_jenis}} </li>
                                <br>
                                <li class="small"><b>Kondisi Buku :</b><span class="fa-li"></span>  
                                    @if ($item->kondisi_buku === "Baik")
                                    <h5><span class="badge badge-success"> {{$item->kondisi_buku}} </span></h5>
                                    @elseif ($item->kondisi_buku === "Cukup Baik")
                                    <h5><span class="badge badge-warning"> {{$item->kondisi_buku}} </span></h5>
                                    @elseif ($item->kondisi_buku === "Rusak")
                                    <h5><span class="badge badge-danger"> {{$item->kondisi_buku}} </span></h5>
                                    @elseif ($item->kondisi_buku === "Hilang")
                                    <h5><span class="badge badge-info"> {{$item->kondisi_buku}} </span></h5>
                                    @else
                                      Tidak ada status
                                    @endif
                                
                                </li>
                                </ul>
                            </div>
                                <div class="col-5 text-center">
                                    <img src="{{asset('gambar-buku/'. $item->thumbnail)}}" alt="user-avatar" class="img-fluid">
                                </div>
                                
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                          <a href="/buku/{{$item->id}}/edit" class="btn btn-sm bg-primary">
                            <i class="fa fa-cog fa-spin fa-1x"></i> Ubah
                          </a>
                          {{-- <a href="#" class="btn btn-sm btn-info">
                            <i class="fas fa-info"></i> Detail
                          </a> --}}
                        </div>
                      </div>
                </div>
            </div>
            @endforeach

        </div>
          

          
       
      <!-- /.card-body -->
      <div class="card-footer">
        <nav aria-label="Contacts Page Navigation">
          <ul class="pagination justify-content-center m-0">
            <li class="page-item active"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">4</a></li>
            <li class="page-item"><a class="page-link" href="#">5</a></li>
            <li class="page-item"><a class="page-link" href="#">6</a></li>
            <li class="page-item"><a class="page-link" href="#">7</a></li>
            <li class="page-item"><a class="page-link" href="#">8</a></li>
          </ul>
        </nav>
      </div>
      <!-- /.card-footer -->
    </div>
    <!-- /.card -->

  </section>

@endsection

