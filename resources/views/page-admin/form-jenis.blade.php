@extends('page-admin.master')

@section('title')

Form Tambah Jenis Buku
    
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

<form action="/jenis" method="POST" enctype="multipart/form-data">
    @csrf   

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Input Jenis Buku</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">

                  <div class="form-group">
                    <label>Jenis Buku Baru</label>
                        <input type="text" class="form-control form-control-lg" name="nama_jenis">
                        @error('nama_jenis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                 
                    </div>
                    
                </div>
                  <div>
                
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>  
              </div>
              <!-- /.card-body -->  
    </section>
    <!-- /.content -->
</form>

@endsection