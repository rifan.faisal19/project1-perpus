@extends('page-admin.master')

@section('title')

Form Tambah Buku
    
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

<form action="/buku" method="POST" enctype="multipart/form-data">
    @csrf   

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Input Buku Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="form-group">
                    <label>Judul Buku</label>
                        <input type="text" class="form-control form-control-lg" name="judul" placeholder="Judul Buku">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Pengarang</label>
                        <input type="text" class="form-control" name="pengarang" placeholder="pengarang">
                        @error('pengarang')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Penerbit</label>
                        <input type="text" class="form-control" name="penerbit" placeholder="Penerbit">
                        @error('penerbit')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <label>Tahun Perolehan</label>
                        <input type="number" min="0" maxlength="4" class="form-control" name="tahun_perolehan" placeholder="Tahun Perolehan">
                        @error('penerbit')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>
                 
                    <div class="form-group">
                        <label>Asal Perolehan </label>
                        <select class="form-control" name="asal_perolehan">
                          <option value="">--Pilih Asal Perolehan--</option>
                          <option value="Dana BOS Regular">Dana BOS Regular</option>
                          <option value="Dana BOS Afirmasi">Dana BOS Afirmasi</option>
                          <option value="Dana BOS Kinerja">Dana BOS Kinerja</option>
                          <option value="Dana Alokasi Khusus (DAK)">Dana Alokasi Khusus (DAK)</option>
                          <option value="Dana Komite">Dana Komite</option>
                          <option value="Dana Lainnya">Dana Lainnya</option>
                        </select>
                        @error('asal_perolehan')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                      <label>Jumlah Buku</label>
                          <input type="number" class="form-control" name="jumlah_buku" placeholder="Jumlah Buku">
                          @error('jumlah_buku')
                              <div class="alert alert-danger">
                                  {{ $message }}
                              </div>
                          @enderror
                    </div>

                    <div class="form-group">
                      <label>Klasifikasi</label>
                      <select class="form-control" name="klasifikasi">
                        <option value="">--Pilih Klasifikasi--</option>
                        <option value="Umum">Umum</option>
                        <option value="Kelas 1">Kelas 1</option>
                        <option value="Kelas 2">Kelas 2</option>
                        <option value="Kelas 3">Kelas 3</option>
                        <option value="Kelas 4">Kelas 4</option>
                        <option value="Kelas 5">Kelas 5</option>
                        <option value="Kelas 6">Kelas 6</option>
                      </select>
                          @error('klasifikasi') 
                              <div class="alert alert-danger">
                                  {{ $message }}
                              </div>
                          @enderror
                    </div>

                    <div class="form-group">
                      <label>Kondisi Buku </label>
                      <select class="form-control" name="kondisi_buku">
                        <option value="">--Pilih Kondisi Buku--</option>
                        <option value="Baik">Baik</option>
                        <option value="Cukup Baik">Cukup Baik</option>
                        <option value="Rusak">Rusak</option>
                        <option value="Hilang">Hilang</option>
                      </select>
                      @error('kondisi_buku')
                      <div class="alert alert-danger">
                          {{ $message }}
                      </div>
                      @enderror
                  </div>

                  <div class="form-group">
                    <label>Jenis Buku </label>
                    <select class="form-control" name="jenis_id">
                      <option value="">--Pilih Jenis Buku--</option>
                        @foreach ($jenis as $item)
                          <option value="{{$item->id}}">{{$item->nama_jenis}}</option>
                        @endforeach
                    </select>
                    @error('jenis_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                   
            <!-- general form elements disabled -->
            <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Upload Photo</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                    <label>File input Cover Buku</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="thumbnail">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                </div>
                  <div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>  
              </div>
              <!-- /.card-body -->  
    </section>
    <!-- /.content -->
</form>

@endsection