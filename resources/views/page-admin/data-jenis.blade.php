@extends('page-admin.master')

@section('title')

Daftar Jenis Buku
    
@endsection

@section('content')



<nav class="navbar navbar-light ">
  <a href="/jenis/create" class="btn btn-primary " > Input Jenis Buku</a>
  <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-light my-2 my-sm-0" type="submit">
      <i class="fa fa-search" aria-hidden="true"></i>
    </button>
  </form>
</nav>



<div class="card-body">
<table class="table table-hover">
    <thead class="table-primary">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Pengunjung</th>
        <th scope="col" style="width: 16%">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($jenis as $key=>$value)
        <tr>
            <td> {{$key + 1}} </td>
            <td> {{$value->nama_jenis}} </td>
            <td> 

              {{-- <form action="/jenis/{{$value->id}}" method="POST">
                @csrf --}}
                  
                  <a href="/jenis/{{$value->id}}/edit" class="btn btn-sm bg-primary" >
                    <i class="fa fa-cog fa-spin fa-1x "></i> Ubah

                  <a></a>

                  {{-- @method('DELETE')
                  <input type="submit" class="btn btn-sm bg-danger" value="Hapus"> --}}
              {{-- </form> --}}
                
                
            </td> 
            
        </tr>
            
            
        @empty
            
        @endforelse
    </tbody>
  </table>
</div>
@endsection