@extends('page-admin.master')

@section('title')

Daftar Anggota 
    
@endsection

@section('content')



<nav class="navbar navbar-light ">
  <a href="/anggota/create" class="btn btn-primary " > Tambah Anggota Baru</a>
  <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-light my-2 my-sm-0" type="submit">
      <i class="fa fa-search" aria-hidden="true"></i>
    </button>
  </form>
</nav>



<div class="card-body">
<table class="table table-hover">
    <thead class="table-primary">
      <tr>
        <th scope="col">#</th>
        <th scope="col">No. Anggota</th>
        <th scope="col">Nama Anggota</th>
        <th scope="col">Tanggal Daftar</th>
        <th scope="col" style="width: 5%">Status</th>
        <th scope="col" style="width: 16%">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($anggota as $key=>$value)
        <tr>
            <td> {{$key + 1}} </td>
            <td> {{$value->no_anggota}} </td>
            <td> {{$value->nama_anggota}} </td>
            <td> {{$value->tgl_daftar}} </td>
            <td> 
                  @if ($value->status_anggota === "Active")
                  <h5><span class="badge badge-success"> {{$value->status_anggota}} </span></h5>
                  @elseif ($value->status_anggota === "Not Active")
                  <h5><span class="badge badge-danger"> {{$value->status_anggota}} </span></h5>
                  @else
                    Tidak ada status
                  @endif
             </td>
            <td> 

              {{-- <form action="/anggota/{{$value->id}}" method="POST">
                @csrf --}}
                  <a href="/anggota/{{$value->id}}" class="btn btn-sm bg-info " >
                    <i class="fas fa-info"></i> Info
                    
                    <a></a>

                  <a href="/anggota/{{$value->id}}/edit" class="btn btn-sm bg-primary" >
                    <i class="fa fa-cog fa-spin fa-1x "></i> Ubah  
                  
                  {{-- @method('DELETE')
                  <input type="submit" class="btn btn-danger" value="Hapus">
              </form> --}}
                
                
            </td> 
            
        </tr>
            
            
        @empty
            
        @endforelse
    </tbody>
  </table>
</div>
@endsection