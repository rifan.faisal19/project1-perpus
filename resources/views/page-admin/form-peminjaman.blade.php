@extends('page-admin.master')

@section('title')

Form Peminjaman Buku
    
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->

<form action="/peminjaman" method="POST" enctype="multipart/form-data">
    @csrf   

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Input Peminjaman Buku</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">

                    <div class="form-group">
                        <label>Tanggal Pinjam</label>
                            <input type="date" class="form-control" name="tgl_pinjam" >
                            @error('tgl_pinjam')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label>Pilih Anggota Peminjaman</label>
                            <select class="form-control" name="anggota_id">
                                <option value="">--Pilih Peminjam--</option>
                                @foreach ($anggota as $item)
                                    <option value="{{$item->id}}">{{$item->nama_anggota}}</option>
                                @endforeach
                            </select>
                            @error('anggota_id')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                      </div>

                      <div class="form-group">
                        <label>Pilih Buku</label>
                            <select class="form-control" name="buku_id">
                                <option value="">--Pilih Buku--</option>
                                @foreach ($buku as $item)
                                    <option value="{{$item->id}}">{{$item->judul}}</option>
                                @endforeach
                            </select>
                            @error('buku_id')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                      </div>
                    
                    <div class="form-group">
                        <label>Tanggal Kembali</label>
                            <input type="date" class="form-control" name="tgl_kembali" >
                            @error('tgl_kembali')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                    </div>

                    <div class="form-group">
                        <label>Status Pinjaman</label>
                        <select class="form-control" name="status_pinjaman">
                          <option value="">--Pilih Status Pinjaman--</option>
                          <option value="Belum Kembali">Belum Kembali</option>
                          <option value="Sudah Kembali">Sudah Kembali</option>
                        </select>
                        @error('status_pinjaman')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                </div>
                    
                </div>
                  <div>
                
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>  
              </div>
              <!-- /.card-body -->  
    </section>
    <!-- /.content -->
</form>

@endsection